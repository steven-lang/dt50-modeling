import org.kramerlab.bmad.algorithms.BooleanMatrixDecomposition;
import org.kramerlab.bmad.general.Tuple;
import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.core.converters.CSVSaver;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Main {
    /**
     * Main
     *
     * @param args cmd args
     * @throws IOException Permission error
     */
    public static void main(String[] args) throws IOException {

        if (args.length < 4) {
            System.err.println("Missing arguments. ");
            System.exit(1);
        }

        // Parse arguments
        System.out.println("Parsing arguments...");
        String matrixPath = args[0];
        int k = Integer.valueOf(args[1]);
        String uOutPath = args[2];
        String vOutPath = args[3];

        // Load matrix
        System.out.println("Reading the matrix...");
        CSVLoader csv = new CSVLoader();
        csv.setSource(new File(matrixPath));
        Instances matrix = csv.getDataSet();
        System.out.println("matrix = " + matrix);
        // Decompose matrix
        System.out.println("Decomposing the matrix");
        final Tuple<Instances, Instances> decomp =
                BooleanMatrixDecomposition
                        .BEST_UNCONFIGURED.decompose(matrix, k);

        // Store the decompositions
        System.out.println("Storing U and V");
        saveCSV(decomp._1, uOutPath);
        saveCSV(decomp._2, vOutPath);

        System.out.println("Finished. Exiting with code.");
    }

    /**
     * Save a given instances object into a path a csv file
     *
     * @param data instances
     * @param path output path
     * @throws IOException No permissions to access path
     */
    private static void saveCSV(Instances data, String path) throws
            IOException {
        CSVSaver csvSaver = new CSVSaver();
        csvSaver.setInstances(data);
        OutputStream os = new FileOutputStream(path);
        csvSaver.setDestination(os);
        csvSaver.writeBatch();
    }
}
