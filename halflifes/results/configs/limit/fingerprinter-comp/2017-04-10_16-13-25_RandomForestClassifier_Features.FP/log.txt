Classification report: 
             precision    recall  f1-score   support

 Above 120d       0.40      0.72      0.52       822
 Below 120d       0.92      0.75      0.82      3479

avg / total       0.82      0.74      0.77      4301

Accuracy score: 0.7416879795396419
Log loss: 0.5712200524418564
Confusion matrix, without normalization
[[ 595  227]
 [ 884 2595]]
Normalized confusion matrix
[[ 0.72  0.28]
 [ 0.25  0.75]]
Estimator: 
RandomForestClassifier(bootstrap=True, class_weight=None, criterion='gini',
            max_depth=None, max_features='auto', max_leaf_nodes=None,
            min_impurity_split=1e-07, min_samples_leaf=1,
            min_samples_split=2, min_weight_fraction_leaf=0.0,
            n_estimators=200, n_jobs=4, oob_score=False, random_state=42,
            verbose=0, warm_start=False) 
Options: 
cache = False 
clfs = ['rf'] 
cv = 10 
data_dir = data 
fp = True 
fpsc = False 
fptype = fp3 
interval = None 
limit = True 
n_jobs = 4 
out = run/ 
regression = False 
sc = False 
test_size = 0.33 
verbose = 0 