Classification report: 
             precision    recall  f1-score   support

 Above 120d       0.70      0.45      0.54       822
 Below 120d       0.88      0.95      0.92      3479

avg / total       0.85      0.86      0.84      4301

Accuracy score: 0.8574750058126017
Log loss: 0.5612224802342854
Confusion matrix, without normalization
[[ 367  455]
 [ 158 3321]]
Normalized confusion matrix
[[ 0.45  0.55]
 [ 0.05  0.95]]
Estimator: 
RandomForestClassifier(bootstrap=True, class_weight=None, criterion='gini',
            max_depth=None, max_features='auto', max_leaf_nodes=None,
            min_impurity_split=1e-07, min_samples_leaf=1,
            min_samples_split=2, min_weight_fraction_leaf=0.0,
            n_estimators=200, n_jobs=4, oob_score=False, random_state=42,
            verbose=0, warm_start=False) 
Options: 
cache = False 
clfs = ['rf'] 
cv = 10 
data_dir = data 
fp = True 
fpsc = False 
fptype = ecfp10 
interval = None 
limit = True 
n_jobs = 4 
out = run/ 
regression = False 
sc = False 
test_size = 0.33 
verbose = 0 