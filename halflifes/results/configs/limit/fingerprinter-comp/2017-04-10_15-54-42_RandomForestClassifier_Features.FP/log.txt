Classification report: 
             precision    recall  f1-score   support

 Above 120d       0.68      0.58      0.63       822
 Below 120d       0.90      0.94      0.92      3479

avg / total       0.86      0.87      0.86      4301

Accuracy score: 0.8681701929783772
Log loss: 0.3833330663157147
Confusion matrix, without normalization
[[ 475  347]
 [ 220 3259]]
Normalized confusion matrix
[[ 0.58  0.42]
 [ 0.06  0.94]]
Estimator: 
RandomForestClassifier(bootstrap=True, class_weight=None, criterion='gini',
            max_depth=None, max_features='auto', max_leaf_nodes=None,
            min_impurity_split=1e-07, min_samples_leaf=1,
            min_samples_split=2, min_weight_fraction_leaf=0.0,
            n_estimators=200, n_jobs=4, oob_score=False, random_state=42,
            verbose=0, warm_start=False) 
Options: 
cache = False 
clfs = ['rf'] 
cv = 10 
data_dir = data 
fp = True 
fpsc = False 
fptype = ecfp2 
interval = None 
limit = True 
n_jobs = 4 
out = run/ 
regression = False 
sc = False 
test_size = 0.33 
verbose = 0 