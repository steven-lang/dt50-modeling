## Fingerprinter comparison
Fingerprinter from `openbabel.pybel` compared
Tested fingerprints are:
- ecfp0
- ecfp10
- ecfp2
- ecfp4
- ecfp6
- ecfp8
- fp2
- fp3
- fp4
- maccs

### Results
| fingerprinter | accuracy | ROC-AUC |
| --- | --- | --- |
| ecfp0 | 0.879 | 0.91 |
| ecfp10 | 0.857 | 0.86 |
| ecfp2 | 0.868 | 0.88 |
| ecfp4 | 0.871 | 0.88 |
| ecfp6 | 0.870 | 0.88 |
| ecfp8 | 0.852 | 0.85 |
| fp2 | 0.876 | 0.91 |
| fp3 | 0.742 | 0.83 |
| fp4 | 0.879 | 0.91 |
| maccs | 0.878 | 0.91 |

For repetition, execute with `FPTYPE` as one of the above
```bash
python src/main_classification.py -d data --clfs rf --limit --fptype FPTYPE -c --features fp scen
```
### Notes
Ran with `RandomForestClassifier` and 5-Fold-CV.