Options: 
batch_size = 64 
cache = True 
compression_rate = 0.6 
cv = None 
data_dir = data 
debug = False 
epochs_dae = 150 
epochs_hl = 100 
fp_encoding_dim = 32 
fptype = maccs 
group_by = fp 
limit = False 
load_models = [] 
n_jobs = 4 
noise_factor = 1.0 
num_switches = 100 
out = run 
regression = True 
scen_encoding_dim = 16 
tag = sr = 100 
test_size = 0.2 
validation_split = 0.25 
verbose = 1 