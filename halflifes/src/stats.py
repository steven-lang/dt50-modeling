import hashlib

import argparse
from pprint import pprint

import matplotlib
from typing import Dict, List
from collections import Counter

import pandas as pd
from scipy.stats import skew
from sklearn.preprocessing import LabelEncoder

from Utils.misc import Feature

matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

from Utils.data_utils import load_dataframe, load_scen_data_encoded, \
    load_fp_data, ensure_dir

plt.style.use('ggplot')

import seaborn as sns
import numpy as np


def parse_arg() -> argparse.Namespace:
    """Parse commandline arguments
    :return: argument dict
    """
    parser = argparse.ArgumentParser('Halflife prediction')
    parser.add_argument('--data-dir', '-d', default='data', type=str,
                        help='Data directory')
    parser.add_argument('--out', '-o', default='run', type=str,
                        help='Output directory')
    parser.add_argument('--cache', '-c', default=False, action='store_true',
                        help='Use the cache')
    parser.add_argument('--verbose', '-v', default=0, type=int,
                        help='Verbosity level')
    parser.add_argument('--n-jobs', '-n', type=int, default=4,
                        help='Number of parallel jobs')
    parser.add_argument('--cv', '-cv', type=int, default=10,
                        help='Number of cross validations')
    parser.add_argument('--regs', type=str, nargs='+',
                        help='Regressor list (seperate by space)')
    parser.add_argument('--features', '-f', type=str, nargs='+', default=[
        'fp', 'scen'], help='Features - can be a list of {"fp", "scen", '
                            '"rule"}. E.g. "fp scen" or "fp scen rule".')
    parser.add_argument('--test-size', type=float, default=0.33,
                        help='Size of the test-set in percent (currently only used in gridsearch)')
    parser.add_argument('--fptype', default='maccs', type=str,
                        help='Fingerprinter type.')
    parser.add_argument('--debug', default=False, action='store_true',
                        help='Enable debug mode')
    parser.add_argument('--mogon', default=False, action='store_true',
                        help='Use mogon mode (disables unsupported libraries).')

    opts = parser.parse_args()

    if opts.out[len(opts.out) - 1] != '/':
        opts.out += '/'

    features = []
    for feat in opts.features:
        if feat == 'fp':
            features.append(Feature.FPS)
        if feat == 'scen':
            features.append(Feature.SCENS)
        if feat == 'rule':
            features.append(Feature.RULES)
    opts.features = features

    return opts


def filter_df(df):

    remove = ['Unnamed: 0', 'compound-id'
        , 'compound-name'
        , 'compound-smiles'
        , 'dt50'
        , 'halflife'
        , 'scenario-id'
        , 'scenario-name'
        , 'scenario-review-status'
        , 'scenario-type'
        , 'spike'
        # , 'exp_moisture_content'
        # , 'biomassDiff'
        , 'soiltexture2_silt'
        , 'soiltexture2_sand'
        , 'soiltexture2_clay']

    for r in remove:
        if r in df:
            del df[r]
    return df

def skewed_vars(opts):
    df, _ = load_dataframe(opts.data_dir, opts.cache, opts)
    df = filter_df(df)

    numeric_feats = df.dtypes[df.dtypes != "object"].index

    skewed_feats = df[numeric_feats].apply(
        lambda x: skew(x.dropna()))  # compute skewness
    skewed_feats_limit_upper = skewed_feats[skewed_feats > 0.75]
    skewed_feats_limit_lower = skewed_feats[skewed_feats < -0.0]
    skewed_feats_index_upper = skewed_feats_limit_upper.index
    skewed_feats_index_lower = skewed_feats_limit_lower.index

    df[skewed_feats_index_upper] = np.log1p(df[skewed_feats_index_upper])
    df[skewed_feats_index_lower] = np.log1p(df[skewed_feats_index_lower])

    print(skewed_feats_limit_upper)
    print(skewed_feats_limit_lower)

    pass

def compounds_stats(opts):
    print()
    print('-'*80)
    print('Compound stats: ')
    df, _ = load_dataframe(opts.data_dir, opts.cache, opts)
    smiles = df['compound-smiles'].values

    alpha = lambda c: c.isalpha()
    remove_alpha = lambda s: list(filter(alpha, s))
    atoms_only = map(remove_alpha, smiles)
    l = list(atoms_only)
    sizes = list(map(lambda s: len(s), l))

    print('Atoms used:')
    print(set(''.join([''.join(s) for s in l])))
    atoms_counts = np.array(sizes)
    print('min: {}'.format(atoms_counts.min()))
    print('max: {}'.format(atoms_counts.max()))
    print('mean: {}'.format(atoms_counts.mean()))
    print('std: {}'.format(atoms_counts.std()))

    X_fp, y = load_fp_data('data', cache=True, opts=parse_arg())
    comps = [hashlib.md5(x.tostring()).digest() for x in X_fp]
    comps = LabelEncoder().fit_transform(comps)
    d: Dict[str, List[float]] = dict()

    # count dt50 values
    for a, b in zip(comps, y):
        if a in d.keys():
            d[a] += [b]
        else:
            d[a] = [b]

    vals = list(d.values())

    freqs = np.array(list(map(len, vals)))

    print('#dt50 == 1 : {}'.format(freqs[freqs == 1].shape[0]))
    print('#dt50 > 5 : {}'.format(freqs[freqs > 5].shape[0]))
    print('#dt50 > 10 : {}'.format(freqs[freqs > 10].shape[0]))

    filt = filter(lambda x: len(x) > 5, vals)
    stds = np.array(list(map(np.std, filt)))

    mean_std = stds.mean()

    print('Mean std: ' + str(mean_std))



    num_pers_dt50 = y[y > 120].shape[0]
    print('Number of persistent dt50s: {}'.format(num_pers_dt50))
    print('Fraction of pesistent dt50s: {}'.format(float(num_pers_dt50)/y.shape[0]))

def num_dt50_over_1000(opts):
    df, _ = load_dataframe(opts.data_dir, opts.cache, opts)
    y = df['dt50']
    bt_1000 = y[y > 1000].shape[0]
    frac = bt_1000 / float(y.shape[0]) * 100
    print(f'{bt_1000} values higher than 1000 days')
    print(f'equals {frac}%')
def scen_stats():
    print()
    print('-'*80)
    print('Scenario stats: ')
    X_scen, y = load_scen_data_encoded('data', cache=True, opts=parse_arg())
    scens = [hashlib.md5(x.tostring()).digest() for x in X_scen]
    scens = LabelEncoder().fit_transform(scens)
    d: Dict[str, List[float]] = dict()

    # count dt50 values
    for a, b in zip(scens, y):
        if a in d.keys():
            d[a] += [b]
        else:
            d[a] = [b]

    vals = list(d.values())

    freqs = np.array(list(map(len, vals)))

    print('#dt50 == 1 : {}'.format(freqs[freqs == 1].shape[0]))
    print('#dt50 > 5 : {}'.format(freqs[freqs > 5].shape[0]))
    print('#dt50 > 10 : {}'.format(freqs[freqs > 10].shape[0]))

    filt = filter(lambda x: len(x) > 5, vals)
    stds = np.array(list(map(np.std, filt)))

    mean_std = stds.mean()

    print('Mean std: ' + str(mean_std))

def print_cont_scenario_table(opts):
    df, _ = load_dataframe(opts.data_dir, opts.cache, opts)
    df = filter_df(df)
    del df['exp_moisture_content']
    del df['biomassDiff']
    print(r"""
\begin{tabular}{llllllll}
\toprule
Parameter & Unit & \% Missing & Minimum & Mean & Median & Maximum & Std. deviation\\
\midrule
    """)

    def print_col_stats(col):
        try:
            column = df[col]
            if column.dtype == np.object:
                return
            if col in ['biomassEnd', 'biomassStart']:
                column = column / 1000.0
            ndigits = 5
            c_min = round(column.min(), ndigits)
            c_mean = round(column.mean(), ndigits)
            c_max = round(column.max(), ndigits)
            c_med = round(column.median(), ndigits)
            c_std = round(column.std(), ndigits)
            column_dropna = column.dropna()
            c_pct_miss = round(1 - column_dropna.shape[0] / float(column.shape[
                                                                      0]),
                               ndigits)
            unit = get_unit(col)
            col = replace_col_name(col)
            col = col.replace('_', '\_')
            print('     {} & {} & {} & {} & {} & {} & {} & {} \\\\ '
                  ''.format(
                col, unit, c_pct_miss, c_min, c_mean, c_med, c_max, c_std
            ))
        except Exception as e:
            print(col + ' ' + repr(e))
            pass

    for col in df:
        print_col_stats(col)

    print(r'    \bottomrule')
    print(r'\end{tabular}')


def replace_col_name(col):
    return {
        'acidity': 'Acidity, pH',
        'biomassEnd': 'Biomass end',
        'biomassStart': 'Biomass start',
        'bulkdensity': 'Bulk density',
        'cec': 'CEC',
        'clay': r'\% clay',
        'humidity': r'\% humidity',
        'omcontent': 'OC',
        'sand': r'\% sand',
        'silt': r'\% silt',
        'spikeconcentration': 'Spike concentration',
        'temperature': 'Temperature',
        'waterstoragecapacity': 'Water storage capacity',
    }.get(col)


def get_unit(col):
    return {
        'acidity': '--',
        'biomassEnd': r'mg C g\textsuperscript{-1}',
        'biomassStart': r'mg C g\textsuperscript{-1}',
        'bulkdensity': r'g cm\textsuperscript{-1}',
        'cec': 'meq./100 g soil',
        'clay': '--',
        'humidity': '--',
        'omcontent': 'g OC/100 g soil',
        'sand': '--',
        'silt': '--',
        'spikeconcentration': 'mg per kg dry soil',
        'temperature': '°C',
        'waterstoragecapacity': 'g water/100 g dry soil',
    }.get(col)


def print_num_cont_cat_cols(opts):
    df, _ = load_dataframe(opts.data_dir, opts.cache, opts)
    df = filter_df(df)
    del df['exp_moisture_content']
    del df['biomassDiff']

    types = {}
    for col in df:
        t = df[col].dtype
        if t in types.keys():
            types[t] += [col]
        else:
            types[t] = [col]
    pprint(types)
    for key, val in types.items():
        print('key: {}, size: {}'.format(key, len(val)))

    count_nominal_values(opts)


def count_nominal_values(opts):
    df, _ = load_dataframe(opts.data_dir, opts.cache, opts)
    df = filter_df(df)
    del df['exp_moisture_content']
    del df['biomassDiff']

    noms = ['redox',
            'soilclassificationsystem',
            'soilsource',
            'soiltexture1']

    for n in noms:
        vals = df[n].values
        unique_vals = set(vals)
        print(n, len(unique_vals), unique_vals)


if __name__ == '__main__':
    # plot_env_vars()
    # plot_dt50_per_comp()

    # compounds_stats(parse_arg())
    # scen_stats()
    arg = parse_arg()
    num_dt50_over_1000(arg)

    # print_cont_scenario_table(arg)
    # print_num_cont_cat_cols(arg)
    # skewed_vars(arg)