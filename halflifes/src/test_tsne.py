import argparse

from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from sklearn.preprocessing import Imputer, StandardScaler

from Utils.data_utils import load_scen_data_encoded
from main_regression import replace_indicator_variables


def parse_arg() -> argparse.Namespace:
    """Parse commandline arguments
    :return: argument dict
    """
    print('Parsing arguments')
    parser = argparse.ArgumentParser('Halflife prediction via Denoising '
                                     'Autoencoders.')
    parser.add_argument('--data-dir', '-d', default='data', type=str,
                        help='Data directory')
    parser.add_argument('--out', '-o', default='run', type=str,
                        help='Output directory')
    parser.add_argument('--cache', '-c', default=False, action='store_true',
                        help='Use the cache')
    parser.add_argument('--verbose', '-v', default=0, type=int,
                        help='Verbosity level')
    parser.add_argument('--n-jobs', '-n', type=int, default=4,
                        help='Number of parallel jobs')
    parser.add_argument('--cv', '-cv', type=int, default=None,
                        help='Number of cross-validations. If none do a '
                             'single holdout.')
    parser.add_argument('--limit', '-l', default=False, action='store_true',
                        help='Start limit classification (120d).')
    parser.add_argument('--regression', '-r', default=False,
                        action='store_true',
                        help='Start regression')
    parser.add_argument('--fptype', default='maccs', type=str,
                        help='Fingerprinter type.')
    parser.add_argument('--debug', default=False, action='store_true',
                        help='Enable debug mode')
    parser.add_argument('--noise-factor', '-nf', default=0.33, type=float,
                        help='Noise-factor')
    parser.add_argument('--test-size', '-ts', default=0.33, type=float,
                        help='Test size.')
    parser.add_argument('--batch-size', '-bs', default=32, type=int,
                        help='Batch size')
    parser.add_argument('--epochs-dae', default=250, type=int,
                        help='Number of epochs')
    parser.add_argument('--epochs-hl', default=250, type=int,
                        help='Number of epochs')
    parser.add_argument('--fp-encoding-dim', '-fped', default=16, type=int,
                        help='Encoding dimension for fingerprints.')
    parser.add_argument('--scen-encoding-dim', '-scened', default=8, type=int,
                        help='Encoding dimension for scenarios.')
    parser.add_argument('--num-switches', '-ns', default=100, type=int,
                        help='Number of switches between training the '
                             'compound autoencoder and the scenario '
                             'autoencoder.')
    parser.add_argument('--tag', '-t', type=str,
                        help='Suffix for the log directory.')
    parser.add_argument('--group-by', '-gb', type=str, default='fp',
                        help='Define the groups for crossvalidation. May be '
                             'one of {"fp", "scen"} or None for no grouping.')
    parser.add_argument('--load-models', type=str, nargs='+', default=[],
                        help='List of models which shall be loaded from cache.'
                             'Can be one of {"fp","scen","hl"}')
    parser.add_argument('--compression-rate', '-cr', type=float, default=0.5,
                        help='Compression rate.')
    parser.add_argument('--validation-split', '-vs', type=float, default=0.05,
                        help='Split of Train/Validation set for network '
                             'training and validation after each epoch.')

    args = parser.parse_args()
    if args.group_by == 'None':
        args.group_by = None
    return args


def plot_tsne(X, y, title):
    # X, y = X[:100], y[:100]
    X = Imputer().fit_transform(X)
    X = StandardScaler().fit_transform(X)
    X = TSNE(random_state=42).fit_transform(X)
    import numpy as np
    y = np.log1p(y)
    y = (y - y.min()) / y.max()
    from matplotlib import pyplot as plt
    plt.style.use('ggplot')

    plt.figure(figsize=(12, 9))
    plt.scatter(X[:,0], X[:,1], c=y)
    plt.savefig(f'{title}.png')
    plt.clf()

def plot_comp():
    X, y = load_scen_data_encoded('data', cache=True, opts=parse_arg(),
                              ret_type='pd')
    plot_tsne(X, y, 'Standard_tsne')
    print('Finished standard')
    replace_indicator_variables(X)
    plot_tsne(X, y, 'Indicator_variables_tsne')

def diff_distance():
    X, y = load_scen_data_encoded('data', cache=True, opts=parse_arg(),
                              ret_type='pd')
    import numpy as np

    def dist(X):
        sum = 0
        for x1 in X:
            for x2 in X:
                sum += np.linalg.norm(x1-x2)
        return sum / X.shape[0]**2

    X = Imputer().fit_transform(X)
    X = StandardScaler().fit_transform(X)

    dist_orig = dist(X)

    X_tsne = TSNE(random_state=42).fit_transform(X)

    dist_tsne = dist(X_tsne)

    X_pca = PCA(n_components=2, random_state=42).fit_transform(X)

    dist_pca = dist(X_pca)


    print(f'Original distance: {dist_orig}')
    print(f'TSNE distance: {dist_tsne}')
    print(f'PCA distance: {dist_pca}')




if __name__ == '__main__':
    # plot_comp()
    diff_distance()