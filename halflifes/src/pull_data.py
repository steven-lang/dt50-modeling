import argparse
import json
from json import JSONDecodeError
from pprint import pprint as print
from time import time

import numpy as np
import requests
from tqdm import tqdm

PREFIX = 'http://localhost:8080/'

decode_error_count = 0


def parse_arg():
    """Parse commandline arguments
    :return: argument dict
    """
    parser = argparse.ArgumentParser('enviPath data puller')
    parser.add_argument('--file', '-f', type=str)
    return parser.parse_args()

def pull_compounds(args: argparse.Namespace):
    # Open session for cookies
    with requests.Session() as s:
        # Request json response
        s.headers.update({'accept': 'application/json'})

        # Login
        s.post(PREFIX + 'package', data=json.load(open(args.file, 'r')))

        bbd_comp_uri = '{}package/32de3cf4-e3e6-4168-956e-32fa5ddb0ce1' \
                       '/compound'.format(PREFIX)

        comps = s.get(bbd_comp_uri).json()['compound']

        res = []

        for c in comps:
            c_res = s.get(c['id']).json()
            res.append({'id':c_res['id'], 'smiles':c_res['smiles']})

def pull(args: argparse.Namespace):
    """
    Pulls the eawag soil package into "data_raw.json"
    :param args: arguments
    :return: None
    """

    # Open session for cookies
    with requests.Session() as s:
        # Request json response
        s.headers.update({'accept': 'application/json'})

        # Login
        s.post(PREFIX + 'package', data=json.load(open(args.file, 'r')))

        # Get the required package
        eawag_soil_uri = '{}package/5882df9c-dae1-4d80-a40e-db4724271456'.format(
            PREFIX)
        package = s.get(eawag_soil_uri).json()

        pws_uri = package['links'][0]['Pathways']

        pws = s.get(pws_uri).json()['pathway']

        data = []

        for pw_obj in tqdm(pws):
            pw_id = pw_obj['id']

            pw = get_json(s, pw_id)
            nodes = pw.get('nodes', [])
            for node_uri in [n['id'] for n in nodes]:
                node = get_json(s, node_uri)
                hls = node.get('halflifes', [])
                for hl in hls:
                    row = {}

                    row['compound-name'] = node.get('name', '?')
                    row['compound-smiles'] = node.get('smiles', '?')
                    row['compound-id'] = node.get('id', '?')
                    scen_id = hl.get('scenarioId', '')
                    row['scenario-id'] = scen_id
                    scen = get_json(s, scen_id)
                    row['scenario-name'] = scen.get('name', '?')
                    row['scenario-type'] = scen.get('type', '?')
                    row['scenario-review-status'] = scen.get('reviewStatus',
                                                             '?')
                    row['dt50'] = hl.get('hl', '')
                    row['dt50-first-order'] = hl.get('hlFirstOrder', '')

                    ad_infos = scen.get('collection', {})
                    parse_add_infos(ad_infos, row)
                    data.append(row)

        print('Number of decode errors: {}'.format(decode_error_count))
        print('Number of entries: {}'.format(len(data)))
        json.dump(data, open('data/data_raw.json', 'w'))


def get_json(session: requests.Session, uri: str):
    try:
        return session.get(uri).json()
    except JSONDecodeError:
        global decode_error_count
        decode_error_count += 1
        return {}
    except Exception:
        print('Something went terribly wrong')
        return {}


def parse_add_infos(infos: dict, row: dict):
    for info_name, info_obj in infos.items():
        for key, val in info_obj.items():
            if key == 'waterstoragecapacityadditionalinformation' and val:
                val = val.split('-')[0].strip()
                if 'nan' in val.lower():
                    val = '?'
            if key == 'omcontentadditionalinformation' and val:
                # Correct omcontent: OC = OM / 1.724 as proposed in eawag paper
                if info_obj['unit'] == 'OM':
                    val = float(val) / 1.724
            row[key] = val


def refactor_data():
    """
    Some additional refactoring applied after the package has been downloaded
    :return:
    """
    data = json.load(open('data/data_raw.json', 'r'))

    # Collect all feature names
    keys = set()
    for row in data:
        keys = keys.union(row.keys())

    feats = np.array(sorted(list(keys)))

    # Fill the data matrix
    X = np.full((len(data), len(feats)), '?', dtype='<U256')
    for row_idx, row in enumerate(data):
        for key, val in row.items():
            feat_idx = np.where(feats == key)[0]
            X[row_idx][feat_idx] = val

    # Write header and data files
    data_file = open('data/data_raw.csv', 'w')

    feats_ = [f.replace('additionalinformation', '') for f in feats]
    data_file.write(','.join(feats_) + '\n')

    for row in X:
        for idx, val in enumerate(row):
            if (isinstance(val, str) or isinstance(val, np.str)) and ',' in val:
                # Replace ','
                row[idx] = val.replace(',', ';')
        j = ','.join(row)
        data_file.write(j + '\n')

    data_file.close()


if __name__ == '__main__':
    args = parse_arg()
    t0 = time()
    pull(args)
    refactor_data()
    print('Took {}s'.format(time() - t0))
    exit(0)
