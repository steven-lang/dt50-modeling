import ujson as json
from argparse import Namespace
from time import gmtime, strftime

import numpy as np
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from sklearn import model_selection
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, \
    GradientBoostingClassifier, RandomForestRegressor, BaggingRegressor
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.kernel_ridge import KernelRidge
from sklearn.linear_model import SGDClassifier, LogisticRegression, \
    LinearRegression, SGDRegressor
from sklearn.model_selection import GridSearchCV
from sklearn.naive_bayes import MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.pipeline import Pipeline
from sklearn.svm import SVC, SVR, LinearSVR
from sklearn.tree import DecisionTreeClassifier, DecisionTreeRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C
from tabulate import tabulate
from typing import List, Iterable, Dict, Tuple
import matplotlib

matplotlib.use('Agg')
import matplotlib.pyplot as plt

plt.style.use('ggplot')

from Utils.data_utils import ensure_dir
from ensembles import EOCClassifier, ClusterEnsemble, TempEstimator

import seaborn as sns

cols = sns.color_palette('deep')
blue = cols[0]
green = cols[1]
red = cols[2]

sns.set(palette="deep", color_codes=True)


def get_reg(type, opts, num_feats):
    """
    Get regressor by type
    :param type: regressor type
    :param opts: options
    :return: regressor
    """
    default = RandomForestRegressor()
    if opts.cv > 0:
        cv = model_selection.GroupKFold(n_splits=opts.cv)
    else:
        cv = model_selection.GroupKFold(n_splits=10)

    gs_params = dict(verbose=opts.verbose,
                     cv=cv,
                     n_jobs=opts.n_jobs,
                     scoring='neg_mean_squared_error')

    if type == 'rfgs':
        print('sqrt features: ', int(np.sqrt(num_feats)))
        print('log2 features: ', int(np.log2(num_feats)))
    temp_idx = 181
    reg = {
        'dt': DecisionTreeRegressor(),
        'rf_full': RandomForestRegressor(n_estimators=100, random_state=42),
        'rf_sqrt': RandomForestRegressor(n_estimators=100, min_samples_split=22,
                                         max_features='sqrt', random_state=42),
        'rf': RandomForestRegressor(n_estimators=100, max_features='sqrt',
                                    random_state=42),
        'rf_log2': RandomForestRegressor(n_estimators=100, max_features='log2',
                                         random_state=42),
        'dual_temp_svr': TempEstimator(temp_idx=temp_idx, base1=SVR(C=2,
                                                                    epsilon=0.3,
                                                                    gamma=0.0078125),
                                       base2=SVR(C=1, epsilon=1.0,
                                                 gamma=0.001953125)),
        'dual_temp_rf': TempEstimator(temp_idx=temp_idx,
                                      base1=RandomForestRegressor(
                                          n_estimators=100, max_features='sqrt',
                                          random_state=42),
                                      base2=RandomForestRegressor(
                                          n_estimators=100, max_features='sqrt',
                                          random_state=42)),
        'dual_temp_bagsvr': TempEstimator(temp_idx=temp_idx,
                                          base1=BaggingRegressor(
                                              base_estimator=SVR(C=2,
                                                                 epsilon=0.3,
                                                                 gamma=0.0078125),
                                              n_estimators=20, max_samples=1.0,
                                              max_features=0.66,
                                              verbose=0, random_state=42),
                                          base2=BaggingRegressor(
                                              base_estimator=SVR(C=1,
                                                                 epsilon=1.0,
                                                                 gamma=0.001953125),
                                              n_estimators=20, max_samples=1.0,
                                              max_features=0.66,
                                              verbose=0, random_state=42)),
        'svr': SVR(C=2, epsilon=0.3, gamma=0.0078125),
        'svr_rbf': SVR(C=2, epsilon=0.3, gamma=0.0078125),
        'svr_rbf_temp': SVR(C=1, epsilon=1.0, gamma=0.001953125),
        'svr_lin': SVR(C=0.125, epsilon=1.0, kernel='linear'),
        'svr_poly': SVR(C=2, epsilon=0.1, degree=3, kernel='poly'),
        'svrgs_lin': GridSearchCV(SVR(),
                                  param_grid={
                                      'C': [2 ** x for x in range(-3, 15, 2)],
                                      'epsilon': [0.1, 0.2, 0.3, 0.4, 0.5, 0.6,
                                                  0.7,
                                                  0.8, 0.9, 1.0],
                                      'kernel': ['linear']
                                  }, **gs_params),
        'svrgs_poly': GridSearchCV(SVR(),
                                   param_grid={
                                       'C': [2 ** x for x in range(-3, 15, 3)],
                                       'epsilon': [0.1, 0.3, 0.5, 0.7, 0.9,
                                                   1.0],
                                       'degree': [2, 3, 4, 5],
                                       'kernel': ['poly']
                                   }, **gs_params),
        'svrgs_rbf': GridSearchCV(SVR(),
                                  param_grid={
                                      'C': [2 ** x for x in range(-3, 15, 3)],
                                      'epsilon': [0.1, 0.3, 0.5, 0.7, 0.9, 1.0],
                                      'gamma': [2 ** x for x in
                                                range(-15, 3, 3)],
                                      'kernel': ['rbf']
                                  }, **gs_params),
        'linsvr': LinearSVR(C=2, epsilon=0.5),
        'bagsvr': BaggingRegressor(base_estimator=SVR(C=2, epsilon=0.3,
                                                      gamma=0.0078125),
                                   n_estimators=20, max_samples=1.0,
                                   max_features=0.66,
                                   verbose=0, random_state=42),
        'bagsvr_temp': BaggingRegressor(
            base_estimator=SVR(C=1, epsilon=1.0, gamma=0.001953125),
            n_estimators=20, max_samples=1.0,
            max_features=0.66,
            verbose=0, random_state=42),
        'rfgs': GridSearchCV(RandomForestRegressor(n_estimators=25,
                                                   random_state=42, ),
                             param_grid={
                                 'min_samples_split': range(2, 202, 10),
                                 'max_features': [5, 10, 25, 50, 100,
                                                  int(np.sqrt(num_feats)),
                                                  int(np.log2(num_feats)),
                                                  num_feats],
                                 'max_depth': range(5, num_feats + 1, 10)
                             }, **gs_params)
    }

    # Exclude the following for mogon runs
    if not opts.mogon and type in ['net', 'xgb', 'xgbgs']:
        from network import HLNetwork
        from xgboost import XGBRegressor
        reg['net'] = HLNetwork(opts, 166, 15)
        reg['xgb'] = XGBRegressor(n_estimators=300, learning_rate=0.1,
                                  max_depth=7, colsample_bylevel=0.9,
                                  colsample_bytree=0.9)
        reg['xgbgs'] = GridSearchCV(XGBRegressor(n_estimators=100,
                                                 colsample_bytree=0.9,
                                                 colsample_bylevel=0.9),
                                    param_grid={
                                        'max_depth': [7, 9, 12],
                                    }, **gs_params)

    return reg.get(type, default)


def get_clf(type, opts):
    """
    Get clf by type
    :param type: clf type
    :param opts: options
    :return: clf
    """

    default = RandomForestClassifier()
    cv = opts.cv
    v = opts.verbose
    clf = {
        'knngs': GridSearchCV(estimator=KNeighborsClassifier(),
                              param_grid={
                                  'n_neighbors': [i for i in range(1, 10, 2)]},
                              verbose=v, n_jobs=opts.n_jobs),
        'svmlinear': SVC(kernel='linear', C=8192.0, probability=True),
        'svmpolygs': GridSearchCV(estimator=SVC(kernel='poly',
                                                probability=False),
                                  param_grid={'degree': [2, 3, 4, 5, 6, 7],
                                              'C': [2 ** x for x in
                                                    range(-3, 15, 2)]},
                                  n_jobs=opts.n_jobs, verbose=v, cv=cv),
        'svmrbf': SVC(kernel='rbf', C=8192.0, gamma=2 ** -3, probability=True,
                      cache_size=500),

        # Gridsearch over C, GAMMA and kernel
        'svmgs': GridSearchCV(SVC(probability=False),
                              param_grid=[
                                  {'C': [2 ** x for x in range(-3, 15, 2)],
                                   'gamma': [2 ** x for x in range(-15, 3, 2)],
                                   'kernel': ['rbf']},
                              ],
                              n_jobs=opts.n_jobs, verbose=v, cv=cv),

        'nb': MultinomialNB(fit_prior=True, alpha=0.78),
        'nbgs': GridSearchCV(MultinomialNB(fit_prior=True),
                             param_grid={
                                 'alpha': [i / 100.0 for i in
                                           np.arange(100, step=3)]
                             }, cv=cv),

        'rf': RandomForestClassifier(n_estimators=200, random_state=42,
                                     criterion='gini', n_jobs=opts.n_jobs),

        'rfdim': GridSearchCV(estimator=Pipeline([
            ('reduce', PCA()),
            ('classify', RandomForestClassifier(n_estimators=200))
        ]), n_jobs=opts.n_jobs, verbose=v, param_grid={
            'reduce__n_components': [i for i in range(10, 150, 5)]
        }),

        'sgdgs': GridSearchCV(estimator=SGDClassifier(shuffle=True,
                                                      learning_rate='optimal',
                                                      n_iter=5000),
                              param_grid={
                                  'penalty': ('l2', 'elasticnet'),
                                  'loss': ('modified_huber', 'log'),
                                  'average': [True, False],
                                  'alpha': 10.0 ** -np.arange(2, 7),
                              }, n_jobs=opts.n_jobs, verbose=v,
                              cv=cv),

        'rfgs': GridSearchCV(estimator=RandomForestClassifier(n_estimators=200),
                             param_grid={
                                 'criterion': ('gini', 'entropy'),
                                 'class_weight': (
                                     'balanced', 'balanced_subsample'),
                                 'max_features': ('log2', 'sqrt')
                             }, n_jobs=opts.n_jobs, verbose=v,
                             cv=cv),

        'logreggs': GridSearchCV(
            estimator=LogisticRegression(), param_grid=[
                {
                    'solver': ['newton-cg', 'lbfgs', 'sag'],
                    'C': [2 ** x for x in range(-3, 19, 2)],
                }, {
                    'solver': ['liblinear'],
                    'intercept_scaling': [x / float(100) for x in range(1, 101,
                                                                        3)],
                    'C': [2 ** x for x in range(-3, 19, 2)],

                }], n_jobs=opts.n_jobs, verbose=v, cv=cv),

        'ada': AdaBoostClassifier(
            base_estimator=DecisionTreeClassifier(random_state=42),
            n_estimators=500,
            random_state=42),
        'adags': GridSearchCV(estimator=AdaBoostClassifier(), param_grid={
            'n_estimators': [50, 100, 250, 1000],
            'base_estimator': [DecisionTreeClassifier(random_state=42)],
            'learning_rate': [0.001, 0.01, 0.1, 1.0, 10, 100],
            'algorithm': ['SAMME', 'SAMME.R']
        }, n_jobs=opts.n_jobs, cv=cv, verbose=1),
        'gb': GradientBoostingClassifier(),
        'gbgs': GridSearchCV(estimator=GradientBoostingClassifier(),
                             param_grid={
                                 'n_estimators': [50, 100, 250, 1000],
                                 'learning_rate': [0.001, 0.01, 0.1, 1.0, 10,
                                                   100],
                             }, n_jobs=opts.n_jobs, cv=cv, verbose=v),
        'sgd': SGDClassifier(),

        'eoc': EOCClassifier(n_jobs=opts.n_jobs)

    }.get(type, default)
    return clf


class Result:
    def __init__(self, est_name: str,
                 y: np.ndarray,
                 y_pred: np.ndarray,
                 histories: dict = None,
                 tag: str = None,
                 opts: Namespace = None):
        self.est_name = est_name
        self.y = list(y)
        self.opts = opts

        # Fix output of some regressors
        if type(y_pred[0]) == np.ndarray:
            y_pred = [v[0] for v in y_pred]
        self.y_pred = list(y_pred)
        self.histories = histories
        self.tag = tag

    def apply(self, metric: staticmethod):
        return metric(self.y, self.y_pred)

    def x_y_to_csv(self, out_dir):
        res = [(yt, yp) for yt, yp in zip(self.y, self.y_pred)]
        np.savetxt('{}/y.csv'.format(out_dir), res, delimiter=',')

    def plot_dist(self, out_dir):

        fig = plt.figure(figsize=(6, 4))
        sns.set(palette="deep", color_codes=True)
        sns.set_style('ticks')
        getter = lambda tup: tup[1]
        zipped = [(yp, yt) for yp, yt in zip(self.y_pred, self.y)]
        s = sorted(zipped, key=getter)
        y_pred = [t[0] for t in s]
        y_true = [t[1] for t in s]

        # plt.title('DT50 distribution: true & predicted')
        x = range(len(y_pred))
        plt.scatter(x=x, y=y_pred, color=blue, s=3, alpha=1.0)
        plt.scatter(x=x, y=y_true, color=red, s=3, alpha=1.0)
        plt.ylim((-6, 10))
        plt.xlabel('Compounds $\\times$ Scenarios')
        plt.ylabel('$\ln(DT_{50})$')
        ax = fig.get_axes()[0]
        ax.set_xticks([])
        plt.tight_layout()
        plt.savefig('{}/{}_iter.png'.format(out_dir, self.est_name), dpi=300)
        plt.close()

    def plot_true_vs_pred(self, out_dir):
        fig = plt.figure(figsize=(6, 4))
        sns.set_style('ticks')
        plt.scatter(x=self.y, y=self.y_pred, color=blue, s=3, alpha=1.0)
        mi = min(self.y + self.y_pred)
        ma = max(self.y + self.y_pred)
        pt = [mi, ma]
        if self.opts.filter_dt50:
            plt.ylim(np.log(1), np.log(320))
            plt.xlim(np.log(1), np.log(320))
        else:
            plt.ylim(-6, 10)
            plt.xlim(-6, 10)
            pass
        plt.plot(pt, pt, color=red, lw=1)
        plt.xlabel('$\ln(DT_{50, true})$')
        plt.ylabel('$\ln(DT_{50, pred})$')
        plt.tight_layout()
        plt.savefig('{}/{}_comp.png'.format(out_dir, self.est_name), dpi=300)
        plt.close()

    def plot_histories(self, out_dir):

        t = ['mean_squared_error', 'loss', 'r2']
        if self.histories is not None:
            for net_part, hist in self.histories.items():
                if hist is None:
                    continue
                for metric in t:
                    ensure_dir('{}/{}/'.format(out_dir, metric))
                    ensure_dir('{}/{}/hists/'.format(out_dir, metric))
                    plt.figure(figsize=(6, 4))
                    # plt.title('{}: epochs vs {}'.format(net_part, metric))
                    y = hist.history[metric]
                    y_val = hist.history['val_' + metric]
                    np.savetxt('{}/{}/hists/val_{}.csv'.format(
                        out_dir, metric, metric), y_val,
                        delimiter=',')
                    np.savetxt('{}/{}/hists/{}.csv'.format(
                        out_dir, metric, metric), y,
                        delimiter=',')

                    plt.plot(y, color=blue, alpha=0.7, label='train')
                    plt.plot(y_val, color=green, alpha=0.7, label='validation')
                    plt.xlabel('epoch')
                    plt.ylabel(metric.replace('_', ' '))
                    plt.legend(loc='upper right', frameon=True, fancybox=False,
                               facecolor='w')
                    plt.savefig('{}/{}/{}_{}.png'.format(
                        out_dir,
                        metric,
                        self.est_name,
                        net_part), dpi=300)
                    plt.close()

    def save(self, out_dir):
        d = '{}/{}'.format(out_dir, self.est_name)
        ensure_dir(d + '/')
        self.plot_dist(d)
        self.plot_true_vs_pred(d)
        self.plot_histories(d)
        self.x_y_to_csv(d)

        try:
            import pickle
            pickle.dump(self, open('{}/res.p'.format(d), 'wb'))
        except Exception as e:
            print(e)

    def load(self, out_dir):
        import pickle
        return pickle.load(open('{}/res.p'.format(out_dir), 'rb'))


class ResultSet:
    def __init__(self, out_dir: str, metrics: List[Tuple[str, staticmethod]],
                 tag: str = None):
        self.results = []  # type: List[Result]
        self.out_dir = out_dir + ('/' + tag if tag else '')
        self.metrics = metrics
        self.tag = tag

    def add_plain(self, y, y_pred, est_name):
        self.add(Result(est_name, y, y_pred))

    def add(self, r: Result):
        self.results.append(r)

    def table(self):
        table = []
        sort = sorted(self.metrics, key=lambda x: x[0])
        header = ['estimator'] + [t[0] for t in sort]
        if self.tag:
            header += ['tag']
        for r in self.results:
            e = [r.est_name] + [m[1](r.y, r.y_pred) for m in sort]
            if self.tag:
                e += [r.tag]
            table.append(e)
        return tabulate(table, header, tablefmt='orgtbl')

    def plot_comparisons(self):

        ensure_dir('{}/{}/'.format(self.out_dir, self.tag))
        for metric in ['mean_squared_error', 'loss', 'r2']:
            for net_part in ['fp', 'scen', 'hl']:
                plt.figure(figsize=(6, 4))
                # plt.title('{}: epochs vs {} ({})'.format(net_part, metric,
                #                                          self.tag))

                for r in self.results:
                    if r.histories[net_part] is None:
                        continue
                    y = r.histories[net_part].history['val_' + metric]
                    plt.plot(y, alpha=0.7, label=r.tag)

                plt.xlabel('epoch')
                plt.ylabel(metric.replace('_', ' '))
                plt.legend(loc="upper right")
                plt.savefig('{}/{}/{}_{}.png'.format(
                    self.out_dir,
                    self.tag,
                    metric,
                    net_part), dpi=300)
                plt.close()

    def save(self):
        ensure_dir(self.out_dir + '/')
        with open('{}/result.txt'.format(self.out_dir), 'w') as f:
            f.write(self.table())
        for r in self.results:
            r.save(self.out_dir)

        if self.results[0].histories is not None:
            self.plot_comparisons()


class ComparisonResult:
    def __init__(self, x, y1, name, xlabel, y1label, log_base=0):
        self.x = x
        self.y1 = y1
        self.name = name
        self.xlabel = xlabel
        self.y1label = y1label
        self.log_base = log_base
        t = strftime("%Y-%m-%d_%H-%M-%S", gmtime())
        self.dir = t + '_' + self.name

    def plot_custom_results(self):
        sns.set(palette="deep", color_codes=True)
        x = self.x
        y1 = self.y1
        fig, ax1 = plt.subplots(figsize=(6, 4))
        ax1.plot(x, y1, c=blue, alpha=0.8)
        ax1.set_xlabel(self.xlabel)
        ax1.yaxis.set_major_formatter(FormatStrFormatter('%.3f'))
        ax1.set_ylabel(self.y1label)
        if self.log_base > 0:
            ax1.set_xscale('log', basex=self.log_base)
        plt.tight_layout()
        plt.savefig('comps/{}/comp.png'.format(self.dir), dpi=300)

    def x_y_to_csv(self):
        res = [(xx, yy1) for xx, yy1 in zip(self.x, self.y1)]
        np.savetxt('comps/{}/xy.csv'.format(self.dir), res, delimiter=',')

    def save(self):
        ensure_dir('comps/' + self.dir + '/')
        self.plot_custom_results()
        self.x_y_to_csv()
