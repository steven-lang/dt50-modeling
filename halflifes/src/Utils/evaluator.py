import itertools
from time import gmtime
from time import strftime

import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.metrics import average_precision_score
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import log_loss
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import roc_curve, auc
from sklearn.preprocessing import LabelBinarizer

from Utils.data_utils import ensure_dir
from Utils.misc import feature_to_str
from Utils.misc import Feature

class Evaluator:
    def __init__(self, clf, features):
        prefix = 'run/' + strftime("%Y-%m-%d_%H-%M-%S",
                                   gmtime()) + '_' + clf.__class__.__name__ +\
                 '_' + feature_to_str(features) + '/'
        ensure_dir(prefix)
        self._prefix = prefix
        self.logfile = 'log'
        self._clf = clf
        pass

    @property
    def logfile(self):
        return self._logfile

    @logfile.setter
    def logfile(self, value):
        self._logfile = open(self._prefix + value + '.txt', 'w')
        pass

    def _print(self, s):
        """
        Prints some stuff to the stdout and a file 'f'
        :param s:
        :return:
        """
        print(s)
        self.logfile.write(str(s) + '\n')

    def _print_metrics(self, y_test, y_pred, y_pred_proba, target_names):
        """
        Prints some metrics about the evaluation
        :param y_test: true labels
        :param y_pred: predicted labels
        :param y_pred_proba: prediction probabilities
        :return:
        """
        out = ''
        out += 'Classification report: \n'
        out += classification_report(y_test, y_pred,
                                     target_names=[str(i) for i in
                                                   target_names])
        out += '\n'
        out += 'Accuracy score: {}\n'.format(accuracy_score(y_test, y_pred))
        out += 'Log loss: {}\n'.format(log_loss(y_test, y_pred_proba))

        # Compute macro-average ROC curve and ROC area
        fpr = dict()
        tpr = dict()
        roc_auc = dict()
        # Binarize the output
        y_test = self._binarize_y(target_names, y_test)
        for i in range(len(target_names)):
            fpr[i], tpr[i], _ = roc_curve(y_test[:, i], y_pred_proba[:, i])
            roc_auc[i] = auc(fpr[i], tpr[i])
            out += 'AUC {}: {:.4}\n'.format(target_names[i], roc_auc[i])
        self._print(out)

    def _plot_cm_curve(self, y_test, y_pred, target_names):
        y_test = [1 if x == target_names[0] else 0 for x in y_test]
        cond_pos = float(len([x for x in y_test if x == 1]))
        cond_neg = float(len([x for x in y_test if x == 0]))

        data = []
        for th in [x / float(100) for x in range(0, 101)]:
            # Skip erroneous thresholds
            if th > 1:
                continue

            y_p = []
            for p0, p1 in y_pred:
                y_p.append(1 if p1 > th else 0)

            cm = confusion_matrix(y_test, y_p)
            tn = cm[0, 0] / cond_neg
            fp = cm[0, 1] / cond_neg
            fn = cm[1, 0] / cond_pos
            tp = cm[1, 1] / cond_pos

            data.append([tn, fp, fn, tp, th])

        data = np.array(data)

        ths = data[:, 4]
        plt.clf()
        plt.figure()
        plt.plot(ths, data[:, 0], label='TN/N')
        plt.plot(ths, data[:, 1], label='FP/N')
        plt.plot(ths, data[:, 2], label='FN/P')
        plt.plot(ths, data[:, 3], label='TP/P')
        # plt.plot(ths, data[:, 0] - data[:, 2], label='TN/N : FN/P')
        plt.title('Confusion Matrix over all thresholds')
        plt.xlabel('Thresholds')
        plt.ylabel('Percentages')
        plt.legend(loc="lower right")
        plt.savefig(self._prefix + 'cm_ths.png')
        plt.clf()

    def _add_plot_roc(self, y_test, y_pred, target_names):
        """
        Adds the roc curve to the plot
        :param y_test: true labels
        :param y_pred: prediction probabilities
        :return:
        """

        plt.subplot(221)
        n_classes = len(target_names)

        # Binarize the output
        y_test = self._binarize_y(target_names, y_test)

        fpr = dict()
        tpr = dict()
        roc_auc = dict()

        # Compute macro-average ROC curve and ROC area
        for i in range(n_classes):
            fpr[i], tpr[i], _ = roc_curve(y_test[:, i], y_pred[:, i])
            roc_auc[i] = auc(fpr[i], tpr[i])

        # Compute micro-average ROC curve and ROC area
        fpr["micro"], tpr["micro"], _ = roc_curve(y_test.ravel(),
                                                  y_pred.ravel())
        roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

        # First aggregate all false positive rates
        all_fpr = np.unique(np.concatenate([fpr[i] for i in range(n_classes)]))

        # Then interpolate all ROC curves at this points
        mean_tpr = np.zeros_like(all_fpr)
        for i in range(n_classes):
            mean_tpr += np.interp(all_fpr, fpr[i], tpr[i])

        # Finally average it and compute AUC
        mean_tpr /= n_classes

        fpr["macro"] = all_fpr
        tpr["macro"] = mean_tpr
        roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])

        # Plot micro and macro averaged curves
        if n_classes > 2:
            plt.plot(fpr["micro"], tpr["micro"],
                     label='micro-average ROC curve (area = {0:0.2f})'
                           ''.format(roc_auc["micro"]),
                     color='deeppink', linestyle=':', linewidth=4)

            plt.plot(fpr["macro"], tpr["macro"],
                     label='macro-average ROC curve (area = {0:0.2f})'
                           ''.format(roc_auc["macro"]),
                     color='navy', linestyle=':', linewidth=4)

        # Plot curve for each class
        for i in range(n_classes):
            plt.plot(fpr[i], tpr[i],
                     label='Class {0} (area = {1:0.2f})'
                           ''.format(target_names[i], roc_auc[i]))

            if n_classes == 2:
                break

        # Plot settings
        plt.plot([0, 1], [0, 1], 'k--')
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title(
            'ROC Curve')
        plt.legend(loc="lower right")

    def _add_plot_confusion_matrix(self, y_test, y_pred, target_names):
        """
        Adds the confusion matrix and the normalized confusion matrix to the plot
        :param y_test: true labels
        :param y_pred: predicted labels
        :return:
        """

        def plot_confusion_matrix(cm, classes,
                                  normalize=False,
                                  title='Confusion matrix',
                                  cmap=plt.cm.Blues):
            """
            This function prints and plots the confusion matrix.
            Normalization can be applied by setting `normalize=True`.
            """
            plt.imshow(cm, interpolation='nearest', cmap=cmap)
            plt.title(title)
            plt.colorbar()
            tick_marks = np.arange(len(classes))
            plt.xticks(tick_marks, classes, rotation=45)
            plt.yticks(tick_marks, classes)

            if normalize:
                cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
                print("Normalized confusion matrix")
            else:
                print('Confusion matrix, without normalization')

            thresh = cm.max() / 2.
            for i, j in itertools.product(range(cm.shape[0]),
                                          range(cm.shape[1])):
                text = '{:5.3f}'.format(cm[i, j]) if normalize else cm[i, j]
                plt.text(j, i, text,
                         horizontalalignment="center",
                         color="white" if cm[i, j] > thresh else "black")

            # plt.tight_layout()
            plt.ylabel('True label')
            plt.xlabel('Predicted label')

        # Compute confusion matrix
        cm = confusion_matrix(y_test, y_pred)
        np.set_printoptions(precision=2)
        self._print('Confusion matrix, without normalization')
        self._print(cm)
        plt.subplot(223)
        plot_confusion_matrix(cm, classes=target_names)

        # Normalize the confusion matrix by row (i.e by the number of samples
        # in each class)
        cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        cm_normalized = np.round(cm_normalized, decimals=2)
        self._print('Normalized confusion matrix')
        self._print(cm_normalized)
        plt.subplot(224)
        plot_confusion_matrix(cm_normalized,
                              title='Normalized confusion matrix',
                              classes=target_names)

    def _add_plot_precision_recall_curve(self, y_test, y_pred, target_names):
        """
        Adds the prc to the plot
        :param y_test: true labels
        :param y_pred: prediction probabilities
        :return:
        """

        # Binarize the output
        n_classes = len(target_names)
        y_test = self._binarize_y(target_names, y_test)
        # Compute Precision-Recall and plot curve
        precision = dict()
        recall = dict()
        average_precision = dict()
        for i in range(n_classes):
            precision[i], recall[i], _ = precision_recall_curve(y_test[:, i],
                                                                y_pred[:, i])
            average_precision[i] = average_precision_score(y_test[:, i],
                                                           y_pred[:, i])

        # Compute micro-average ROC curve and ROC area
        precision["micro"], recall["micro"], _ = precision_recall_curve(
            y_test.ravel(),
            y_pred.ravel())
        average_precision["micro"] = average_precision_score(y_test, y_pred,
                                                             average="micro")

        # Plot Precision-Recall curve
        plt.subplot(222)

        # plt.plot(recall[0], precision[0], color='navy',
        #          label='Precision-Recall curve')
        plt.xlabel('Recall')
        plt.ylabel('Precision')
        plt.ylim([0.0, 1.05])
        plt.xlim([0.0, 1.0])
        plt.title('Precision-Recall example: AUC={0:0.2f}'.format(
            average_precision[0]))
        plt.legend(loc="lower left")

        if n_classes > 2:
            # Plot Precision-Recall curve for each class
            plt.plot(recall["micro"], precision["micro"], color='gold',
                     label='micro-average Precision-recall curve (area = {0:0.2f})'
                           ''.format(average_precision["micro"]))

        # setup plot details
        for i in range(n_classes):
            plt.plot(recall[i], precision[i],
                     label='Class {0} (area = {1:0.2f})'
                           ''.format(target_names[i], average_precision[i]))

            if n_classes == 2:
                break

        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('Recall')
        plt.ylabel('Precision')
        plt.title('Precision-Recall curve')
        plt.legend(loc="lower right")

    def _binarize_y(self, target_names, y):
        """Binarize the target value y"""
        n_classes = len(target_names)
        if n_classes > 2:
            lb = LabelBinarizer()
            lb.fit(y)
            y = lb.transform(y)
        else:
            # Limit case
            # limit target names are: ['Above 120d', 'Below 120d']
            # Clf probs are [above_prob, below_prob]
            y = np.array(
                [[0, 1] if y == target_names[1] else [1, 0] for y in y])

        return y



    def evaluate_clf(self, y_test, y_pred, y_pred_proba, target_names, opts):
        """
        Starts the evaluation process
        :param y_test: true labels
        :param y_pred: predicted labels
        :param y_pred_proba: prediction probabilities
        :return:
        """

        plt.figure(figsize=(16, 12))

        # y_pred = [str(y) for y in y_pred]
        # y_test = [str(y) for y in y_test]

        # Print metrics
        self._print_metrics(y_test, y_pred, y_pred_proba, target_names)

        # Plot roc curve
        self._add_plot_roc(y_test, y_pred_proba, target_names)

        # Plot confusion matrix
        self._add_plot_confusion_matrix(y_test, y_pred, target_names)

        # Plot precision recall curve
        self._add_plot_precision_recall_curve(y_test, y_pred_proba,
                                              target_names)

        plt.savefig(self._prefix + 'eval.png')

        # Only for binary classification
        if len(target_names) == 2:
            self._plot_cm_curve(y_test, y_pred_proba, target_names)

        self.logfile.write('Estimator: \n')
        self.logfile.write(str(self._clf) + ' \n')

        self.logfile.write('Options: \n' + '\n'.join(['{} = {} '.format(
            x,y ) for x,y in sorted(vars(opts).items())]))

        self.logfile.close()

        # self.generate_report(target_names, y_pred, y_pred_proba, y_test)

        pred_file = open(self._prefix + 'predictions.csv', 'w')
        pred_file.write('y_test, y_hat\n')
        for yt, yh in zip(y_test, y_pred):
            pred_file.write('{}, {}\n'.format(yt, yh))
        pred_file.close()
