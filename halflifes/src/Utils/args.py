import argparse
from argparse import Namespace

from Utils.misc import Feature


def parse_arg() -> Namespace:
    """Parse commandline arguments
    :return: argument dict
    """
    parser = argparse.ArgumentParser('Halflife prediction')
    parser.add_argument('--data-dir', '-d', default='data', type=str,
                        help='Data directory')
    parser.add_argument('--cache', '-c', default=False, action='store_true',
                        help='Use the cache')
    parser.add_argument('--verbose', '-v', default=0, type=int,
                        help='Verbosity level')
    parser.add_argument('--n-jobs', '-n', type=int, default=4,
                        help='Number of parallel jobs')
    parser.add_argument('--cv', '-cv', type=int, default=10,
                        help='Number of cross validations')
    parser.add_argument('--regs', type=str, nargs='+',
                        help='Regressor list (separate by space)')
    parser.add_argument('--test-size', type=float, default=0.33,
                        help='Size of the test-set in percent (currently only used in gridsearch)')
    parser.add_argument('--fptype', default='maccs', type=str,
                        help='Fingerprinter type.')
    parser.add_argument('--debug', default=False, action='store_true',
                        help='Enable debug mode')
    parser.add_argument('--mogon', default=False, action='store_true',
                        help='Use mogon mode (disables unsupported libraries).')
    parser.add_argument('--epochs-dae', default=250, type=int,
                        help='Number of epochs')
    parser.add_argument('--epochs-hl', default=250, type=int,
                        help='Number of epochs')
    parser.add_argument('--fp-encoding-dim', '-fped', default=16, type=int,
                        help='Encoding dimension for fingerprints.')
    parser.add_argument('--scen-encoding-dim', '-scened', default=8, type=int,
                        help='Encoding dimension for scenarios.')
    parser.add_argument('--num-switches', '-ns', default=100, type=int,
                        help='Number of switches between training the '
                             'compound autoencoder and the scenario '
                             'autoencoder.')
    parser.add_argument('--tag', '-t', type=str,
                        help='Suffix for the log directory.')
    parser.add_argument('--compression-rate', '-cr', type=float, default=0.5,
                        help='Compression rate.')
    parser.add_argument('--lr', '-lr', type=float, default=0.01,
                        help='Learning rate for Adam optimizer.')
    parser.add_argument('--validation-split', '-vs', type=float, default=0.05,
                        help='Split of Train/Validation set for network '
                             'training and validation after each epoch.')
    parser.add_argument('--limit', '-l', default=False, action='store_true',
                        help='Start limit classification (120d).')
    parser.add_argument('--regression', '-r', default=False,
                        action='store_true',
                        help='Start regression')
    parser.add_argument('--batch-size', '-bs', default=32, type=int,
                        help='Batch size')
    parser.add_argument('--noise-factor', '-nf', default=0.33, type=float,
                        help='Noise-factor')
    parser.add_argument('--filter-scenarios', default=False,
                        action='store_true', help='Filter scenario features.')
    parser.add_argument('--filter-dt50', default=False,
                        action='store_true', help='Filter dt50 values in 1 < '
                                                  'x < 320.')
    parser.add_argument('--filter-df', default=False,
                        action='store_true', help='Filter dataframe on '
                                                  'different conditions.')
    parser.add_argument('--skew-thres', default=0.75, type=float,
                        help='Skewness fix threshold.')
    parser.add_argument('--filter-dt50-perc', default=None, type=float,
                        help='Filter percentiles.')
    parser.add_argument('--imputer', default='MEAN', type=str,
                        help='Imputing strategy')
    parser.add_argument('--min-num-scen', default=0, type=int,
                        help='Filter compounds by number of scenarios.')
    parser.add_argument('--kernel-reg', '-kr', type=float, default=0.001,
                        help='Kernel regularizer scale.')
    parser.add_argument('--act-reg', '-ar', type=float, default=0.0001,
                        help='Activation regularizer scale.')
    parser.add_argument('--patience', '-p', type=int, default=5,
                        help='Early stopping patience.')
    parser.add_argument('--drop-temp', '-dt', default=False,
                        action='store_true',
                        help='Drop temperature scenarios other than 20 degree.')
    parser.add_argument('--drop-temp-inv', '-dti', default=False,
                        action='store_true',
                        help='Drop temperature scenarios with 20 degree.')

    opts = parser.parse_args()

    if opts.filter_scenarios and opts.cache:
        print('WARNING: CACHE WAS ENABLED BUT FILTERING SCENARIOS MAY NEEDS '
              'CACHE TO BE DISABLED TO WORK PROPERLY!')
    return opts