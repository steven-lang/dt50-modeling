import argparse
import os

import sys
from time import strftime, gmtime, time

import pandas as pd
import pybel
from scipy.stats import skew
from sklearn import model_selection, clone, metrics
from sklearn.ensemble import RandomForestRegressor
from sklearn.kernel_ridge import KernelRidge
from sklearn.metrics import mean_absolute_error, mean_squared_error
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import Imputer, StandardScaler
from sklearn.svm import SVR
from sklearn.utils import shuffle

from Utils.args import parse_arg

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from Utils.data_utils import make_groups, save_opts, ScenarioScaler, \
    load_comp_descs, CustomImputer
from Utils.model_util import ResultSet, Result
import numpy as np
from Utils.data_utils import ensure_dir, load_fp_data, load_scen_data_encoded
from network import HLNetwork

CURRENT_TIME_STR = strftime("%Y-%m-%d_%H-%M-%S", gmtime())
PREFIX = 'run/dae/' + CURRENT_TIME_STR + '_Regression_Network'


def run(opts: argparse.Namespace, tag) -> Result:
    """Main method"""

    # Get the data
    data_dir = opts.data_dir
    X_fp, y = load_fp_data(data_dir, opts.cache, opts)
    X_scen, y = load_scen_data_encoded(data_dir, opts.cache, opts)
    X_comp_desc, y = load_comp_descs(opts)

    print()
    print('Shapes: ')
    print('X_fp.shape = {}'.format(X_fp.shape))
    print('X_comp_desc.shape = {}'.format(X_comp_desc.shape))
    print('X_scen.shape = {}'.format(X_scen.shape))
    print()

    # Prepare the target variable
    if opts.limit:
        # Boolean target: below 120d or above
        y = y < 120
    elif opts.regression:
        # Regression: predict the dt50 value
        y = np.log(y)
        y[y == -np.inf] = y[y != -np.inf].min()


    # Stack the environment and fingerprint features together to be
    # applicable to the .fit(..) method of sklearn estimators
    X = np.hstack((X_fp, X_comp_desc, X_scen))




    # Init the network and a data-pipeline
    net = HLNetwork(opts, fp_dim=X_fp.shape[1],
                    comp_desc_dim=X_comp_desc.shape[1])

    pipe = Pipeline(
        steps=[
            ('imputer', CustomImputer(fp_dim=X_fp.shape[1], type=opts.imputer)),
            # ('imputer', Imputer(strategy='mean')),
            ('scaler', StandardScaler()),
            # ('distmapper', ScenarioDistanceTransformer(fp_dim=fp_dim)),
            # ('net', HLNetwork(opts, fp_dim, comp_desc_dim)),
            ('reg', net)])

    if opts.cv is not None and opts.cv > 1:
        # Cross validated run
        rs = model_selection.GroupKFold(n_splits=opts.cv)

        # Create groups from hashes of the rows
        groups = make_groups(X_fp, X_scen)
        X, y, groups = shuffle(X, y, groups, random_state=42)
        y_pred = model_selection.cross_val_predict(estimator=pipe,
                                                   X=X,
                                                   y=y,
                                                   cv=rs,
                                                   n_jobs=opts.n_jobs,
                                                   groups=groups)
        hists = None
    else:
        # Single run with one train and one test set
        rs = model_selection.GroupShuffleSplit(n_splits=1,
                                               test_size=opts.test_size,
                                               random_state=42)

        # Create groups from hashes of the rows
        groups = make_groups(X_fp, X_scen)
        X, y, groups = shuffle(X, y, groups, random_state=42)
        [(train, test)] = rs.split(X, y, groups)
        X_train, X_test, y_train, y_test = X[train], X[test], y[train], y[test]
        pipe.fit(X_train, y_train)
        y_pred = pipe.predict(X_test)
        y = y_test

        hists = net.histories()

    return Result(est_name=tag, y=y, y_pred=y_pred, histories=hists,
                  tag=opts.tag, opts=opts)



if __name__ == '__main__':
    opts = parse_arg()
    ensure_dir("run/dae/")

    scorings = [('MSE', metrics.mean_squared_error),
                ('MAE', metrics.mean_absolute_error),
                ('MedAE', metrics.median_absolute_error),
                ('R2', metrics.r2_score),
                ('ExplVar', metrics.explained_variance_score)]

    rs = ResultSet(PREFIX, scorings, tag='')

    rs.add(run(opts, opts.tag))
    rs.save()
    save_opts(PREFIX, opts)
