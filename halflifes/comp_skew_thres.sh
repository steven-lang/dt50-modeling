#!/bin/bash
thress=( 0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0 )
for th in ${thress[@]} 
do
	echo ">>>>>>>>>>>>>>> Current threshold: $th"
	./env/bin/python src/main_regression.py --regs rf_sqrt svr bagsvr --n-jobs 4 --cv 10 --skew-thres $th --tag $th
done
