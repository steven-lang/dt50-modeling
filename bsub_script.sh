#!/bin/sh
#BSUB -q nodeshort
#BSUB -J svmgs[1-3]
#BSUB -app Reserve1800M
#BSUB -n 64
#BSUB -R 'span[ptile=64]'
#BSUB -W 5:00

ID=`echo "$LSB_JOBINDEX " | bc`

options=`cat options | head -n $ID | tail -n 1`

module load Python/3.5_mkl 
cd ~/bachelor-thesis/halflifes
./env/bin/python src/main_classification.py -d data -c -n 58 $options --clfs svmgs
