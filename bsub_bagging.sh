#!/bin/sh
#BSUB -q nodeshort
#BSUB -app Reserve500M
#BSUB -n 64
#BSUB -R 'span[ptile=64]'
#BSUB -W 2:00

# ID=`echo "$LSB_JOBINDEX " | bc`

# options=`cat options_gs | head -n $ID | tail -n 1`

module load Python/3.5_mkl 
cd ~/bachelor-thesis/halflifes
./env/bin/python src/main_regression.py -d data --regs rf_full rf_log2 rf_sqrt bagsvr_log2 bagsvr_sqrt bagsvr_0.2 -c --n-jobs 60 --cv 10 --verbose 10 --mogon
