#!/bin/sh
#BSUB -q nodeshort
#BSUB -J logreg
#BSUB -app Reserve500M
#BSUB -n 64
#BSUB -R 'span[ptile=64]'
#BSUB -W 1:00

module load Python/3.5_mkl 
cd ~/bachelor-thesis/halflifes
./env/bin/python src/main_classification.py -d data --clfs logreggs --verbose 3 --limit -f fp scen -c --cv 10 --n-jobs 60

