#!/bin/sh
#BSUB -q nodeshort
#BSUB -app Reserve500M
#BSUB -n 64
#BSUB -R 'span[ptile=64]'
#BSUB -W 1:30

module load Python/3.5_mkl 
cd ~/bachelor-thesis/halflifes
./env/bin/python src/main_regression.py -d data --regs rf kr -c --n-jobs 60 --mogon
