#!/bin/sh
#BSUB -q nodeshort
#BSUB -app Reserve500M
#BSUB -n 64
#BSUB -R 'span[ptile=64]'
#BSUB -W 3:00

module load Python/3.5_mkl 
cd ~/bachelor-thesis/halflifes
./env/bin/python src/main_permutation_test.py -d data -c --n-jobs 60 --mogon --cv 10 -v 10 -c -nt 500 --mogon
